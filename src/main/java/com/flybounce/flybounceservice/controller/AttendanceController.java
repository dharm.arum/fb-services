package com.flybounce.flybounceservice.controller;

import com.flybounce.flybounceservice.model.Attendance;
import com.flybounce.flybounceservice.model.User;
import com.flybounce.flybounceservice.reports.GenerateAttendanceReport;
import com.flybounce.flybounceservice.repositories.AttendanceRepository;
import com.flybounce.flybounceservice.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/attendance")
public class AttendanceController {

    Logger logger = LoggerFactory.getLogger(AttendanceController.class);

    @Autowired
    AttendanceRepository attendanceRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @PostMapping
    public Attendance saveAttendance(@RequestBody Attendance attendance) {
        return attendanceRepository.save(attendance);
    }

    @RequestMapping(value = "/generateAttendanceReport", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamSource> attendanceReport(@RequestParam Long startDate, @RequestParam(required = false) Long endDate) {
        if (Objects.isNull(endDate)) {
            endDate = new Date().getTime();
        }
        Iterable<Attendance> attendanceList = attendanceRepository.findByStartdateBetweenOrderByStartdate(startDate, endDate);
        ByteArrayInputStream stream = GenerateAttendanceReport.attendanceReport(userRepository, mongoTemplate, attendanceList, startDate, endDate);
        //
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=attendance-summary-report.pdf");
        //
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(stream));
    }
}
