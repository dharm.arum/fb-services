package com.flybounce.flybounceservice.controller;

import com.flybounce.flybounceservice.model.User;
import com.flybounce.flybounceservice.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/login")
public class AuthController {

    Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PostMapping()
    public User login(@RequestBody User user) throws Exception{
        User loggedInUser = userRepository.findByEmail(user.getEmail());
        if(Objects.isNull(loggedInUser)) {
            throw new Exception("Logged In User not found");
        }
        if(!passwordEncoder.matches(user.getPassword(), loggedInUser.getPassword())) {
            throw new Exception("Invalid Password");
        }
        return loggedInUser;
    }
}
