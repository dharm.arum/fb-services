package com.flybounce.flybounceservice.scheduler;

import com.flybounce.flybounceservice.model.MonthlyBurnOut;
import com.flybounce.flybounceservice.model.AttendanceCoachingBurnOut;
import com.flybounce.flybounceservice.model.MonthlyCoachingBurnOut;
import com.flybounce.flybounceservice.model.User;
import com.flybounce.flybounceservice.repositories.MonthlyBurnOutRepository;
import com.flybounce.flybounceservice.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    MonthlyBurnOutRepository monthlyBurnOutRepository;

    @Autowired
    UserRepository userRepository;
    // 0 * * * * ?
    // 0 0 9 1 * ? // run at 9am, 1st of every month
    @Scheduled(cron = "0 0 9 1 * ?")
    public void scheduleTaskWithCronExpression() {
        logger.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));

        Calendar aCalendar = Calendar.getInstance();
        aCalendar.add(Calendar.MONTH, -1);
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.set(Calendar.HOUR, 1);
        aCalendar.set(Calendar.MINUTE, 0);
        aCalendar.set(Calendar.SECOND, 0);
        aCalendar.set(Calendar.MILLISECOND, 0);

        Date from = aCalendar.getTime();
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        aCalendar.set(Calendar.HOUR, 23);
        aCalendar.set(Calendar.MINUTE, 0);
        aCalendar.set(Calendar.SECOND, 0);
        aCalendar.set(Calendar.MILLISECOND, 0);
        Date to = aCalendar.getTime();

        Aggregation agg = newAggregation(
                match(Criteria.where("startdate").gte(from.getTime()).lte(to.getTime())),
                project("userid")
                        .and("userid").as("userId")
                        .and("enddate").minus("startdate").as("burnOut"),
                group("userid")
                        .sum("burnOut").as("burnOut"),
                sort(new Sort(Sort.Direction.DESC, "burnOut"))
        );
        AggregationResults<AttendanceCoachingBurnOut> aggregationResults = mongoTemplate.aggregate(agg, "attendance", AttendanceCoachingBurnOut.class);

        monthlyBurnOutRepository.deleteByStartdate(from.getTime());
        MonthlyBurnOut burnOut = new MonthlyBurnOut();
        burnOut.setStartdate(from.getTime());
        burnOut.setEnddate(to.getTime());
        //
        List<MonthlyCoachingBurnOut> monthlyCoachingBurnOuts = new ArrayList<>();
        for (AttendanceCoachingBurnOut attendanceCoachingBurnOut : aggregationResults) {
            long burnOutDiff = attendanceCoachingBurnOut.getBurnOut();
            double burnOutDiffInHrs = (double) burnOutDiff / 3600000;
            burnOutDiffInHrs = Math.round(burnOutDiffInHrs * 100.0) / 100.0;
            String userName = "";
            if(Objects.nonNull(attendanceCoachingBurnOut.getUserId())) {
                Optional<User> user = userRepository.findById(attendanceCoachingBurnOut.getUserId());
                if(user.isPresent()) {
                    userName = user.get().getName();
                    if(burnOutDiffInHrs > 0) {
                        MonthlyCoachingBurnOut monthlyCoachingBurnOut = new MonthlyCoachingBurnOut();
                        monthlyCoachingBurnOut.setUsername(userName);
                        monthlyCoachingBurnOut.setUserId(attendanceCoachingBurnOut.getUserId());
                        monthlyCoachingBurnOut.setBurnOut(attendanceCoachingBurnOut.getBurnOut());
                        monthlyCoachingBurnOut.setFee(0L);
                        monthlyCoachingBurnOut.setBankreference("");
                        monthlyCoachingBurnOuts.add(monthlyCoachingBurnOut);
                    }
                }
            };
        };
        burnOut.setBurnout(monthlyCoachingBurnOuts);
        //
        monthlyBurnOutRepository.save(burnOut);
        logger.info("Monthly Aggregation Success");
    }
}
