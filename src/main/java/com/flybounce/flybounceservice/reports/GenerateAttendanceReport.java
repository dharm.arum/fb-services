package com.flybounce.flybounceservice.reports;

import com.flybounce.flybounceservice.model.Attendance;
import com.flybounce.flybounceservice.model.AttendanceCoachingBurnOut;
import com.flybounce.flybounceservice.model.User;
import com.flybounce.flybounceservice.repositories.UserRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.TimeZone;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;


public class GenerateAttendanceReport {
    private static final Logger logger = LoggerFactory.getLogger(GenerateAttendanceReport.class);

    public static ByteArrayInputStream attendanceReport(UserRepository userRepository, MongoTemplate mongoTemplate, Iterable<Attendance> attendances, Long from, Long to) {
        Document document = new Document();
        Date now = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        hourFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        DateFormat titleDateFormat = new SimpleDateFormat("MMMM yyyy");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //
        Aggregation agg = newAggregation(
                match(Criteria.where("startdate").gte(from).lte(to)),
                project("userid")
                        .and("enddate").minus("startdate").as("burnOut"),
                group("userid")
                        .sum("burnOut").as("burnOut"),
                sort(new Sort(Sort.Direction.DESC, "burnOut"))
        );

        AggregationResults<AttendanceCoachingBurnOut> aggregationResults = mongoTemplate.aggregate(agg, "attendance", AttendanceCoachingBurnOut.class);

        try {
            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100f);
            table.setWidths(new int[]{2, 1, 1, 2});
            table.setSpacingBefore(10);
            table.setSpacingAfter(40);
            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Student Name", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Coaching Type", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Duration", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Coached Time", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            for (Attendance attendance: attendances ) {
                PdfPCell cell;

                Date startDate = new Date(attendance.getStartdate());
                Date endDate = new Date(attendance.getEnddate());
                long diff = endDate.getTime() - startDate.getTime();
                long diffInHrs = diff / (60 * 60 * 1000) % 24;
                long diffMinutes = diff / (60 * 1000) % 60;

                cell = new PdfPCell(new Phrase(attendance.getUsername()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(attendance.getCoachingtype()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                if (attendance.isIsabsent()) {
                    Font font = new Font();
                    font.setColor(BaseColor.RED);
                    cell = new PdfPCell(new Phrase("Absent", font));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                } else if (attendance.isNoclass()){
                    Font font = new Font();
                    font.setColor(BaseColor.RED);
                    cell = new PdfPCell(new Phrase("No Class because of " + attendance.getNoclassreason(), font));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                } else {
                    cell = new PdfPCell(new Phrase(diffInHrs + "hrs " + diffMinutes + "min "));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                }
                cell = new PdfPCell(new Phrase(dateFormat.format(startDate) + " " + hourFormat.format(startDate) + " - " + hourFormat.format(endDate)));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            //add summary table
            PdfPTable summaryTable = new PdfPTable(2);
            summaryTable.setWidthPercentage(100f);
            summaryTable.setWidths(new int[]{1, 1});
            summaryTable.setSpacingBefore(10);

            hcell = new PdfPCell(new Phrase("Student name", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            summaryTable.addCell(hcell);
            /*
            hcell = new PdfPCell(new Phrase("Phone number", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            summaryTable.addCell(hcell);*/

            hcell = new PdfPCell(new Phrase("Total hours burnt in the court", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            summaryTable.addCell(hcell);

            for (AttendanceCoachingBurnOut burnOut: aggregationResults) {
                PdfPCell cell;

                long burnOutDiff = burnOut.getBurnOut();
                double burnOutDiffInHrs = (double) burnOutDiff / 3600000;
                burnOutDiffInHrs = Math.round(burnOutDiffInHrs * 100.0) / 100.0;
                // String phoneNumber = "";
                String userName = "";
                if(Objects.nonNull(burnOut.getUserId())) {
                    Optional<User> user = userRepository.findById(burnOut.getUserId());
                    if(user.isPresent()) {
                        // phoneNumber = user.get().getPhonenumber();
                        userName = user.get().getName();
                        if(burnOutDiffInHrs > 0) {
                            cell = new PdfPCell(new Phrase(userName));
                            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            summaryTable.addCell(cell);

                            String phrase = burnOutDiffInHrs + "hrs ";
                            cell = new PdfPCell(new Phrase(phrase));
                            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            summaryTable.addCell(cell);
                        }
                    }
                };
                //
            }
            //summary table ends here...
            PdfWriter writer = PdfWriter.getInstance(document, out);
            writer.setPageEvent(new HeaderFooterPageEvent());
            //
            document.open();
            document.addTitle("Monthly Attendance Report - " + titleDateFormat.format(now));
            //
            document.add(new Paragraph("Monthly Summary Report - " + dateFormat.format(from) + " to " + dateFormat.format(to), headFont));
            document.add(summaryTable);
            //
            document.add(new Paragraph("Monthly Attendance Report - " + dateFormat.format(from) + " to " + dateFormat.format(to), headFont));
            document.add(table);
            //
            document.close();

        } catch (DocumentException e) {
            logger.error("Error in pdf generation");
        }
        return new ByteArrayInputStream(out.toByteArray());
    }
}
