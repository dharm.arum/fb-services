package com.flybounce.flybounceservice.reports;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class HeaderFooterPageEvent extends PdfPageEventHelper {

    private static final Logger logger = LoggerFactory.getLogger(HeaderFooterPageEvent.class);

    public void onStartPage(PdfWriter writer, Document document) {
        String img = "classpath:images/flybounce-logo.png";
        Image image;
        try {
            image = Image.getInstance(img);
            image.setAlignment(Element.ALIGN_RIGHT);
            image.setAbsolutePosition(20, 810);
            image.scalePercent(7.5f, 7.5f);
            writer.getDirectContent().addImage(image, true);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(""), 30, 820, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("https://www.flybouncesports.com"), 500, 820, 0);
    }

    public void onEndPage(PdfWriter writer, Document document) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:MM");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Report generated at " + dateFormat.format(new Date())), 110, 30, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("page " + document.getPageNumber()), 550, 30, 0);
    }

}
