package com.flybounce.flybounceservice.repositories;

import com.flybounce.flybounceservice.model.MonthlyBurnOut;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MonthlyBurnOutRepository extends CrudRepository<MonthlyBurnOut, String> {
    List<MonthlyBurnOut> deleteByStartdate(Long startdate);
}