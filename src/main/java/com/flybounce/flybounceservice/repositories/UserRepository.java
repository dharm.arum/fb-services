package com.flybounce.flybounceservice.repositories;

import com.flybounce.flybounceservice.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {
    User findByEmail(String email);
    List<User> findAllByOrderByNameAsc();
}
