package com.flybounce.flybounceservice.repositories;

import com.flybounce.flybounceservice.model.Attendance;
import com.flybounce.flybounceservice.model.User;
import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, String> {
    Iterable<Attendance> findByStartdateBetweenOrderByStartdate(Long startdate, Long enddate);
}
