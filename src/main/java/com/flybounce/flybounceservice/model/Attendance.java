package com.flybounce.flybounceservice.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Document(collection = "attendance")
public class Attendance {
    @Id
    private String id;
    private Long startdate;
    private Long enddate;
    private String userid;
    private String username;
    private boolean noclass = false;
    private String noclassreason;
    private boolean isabsent = false;
    private String coachingtype;
}
