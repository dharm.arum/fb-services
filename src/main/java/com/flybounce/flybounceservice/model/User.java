package com.flybounce.flybounceservice.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "users")
public class User {
    @Id
    private String id;
    private String name;
    private String email;
    private String address;
    private String password;
    private int enabled = 1;
    private String role;
    private String phonenumber;
}