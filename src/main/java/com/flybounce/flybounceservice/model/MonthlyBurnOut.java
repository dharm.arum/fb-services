package com.flybounce.flybounceservice.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "monthlyburnout")
public class MonthlyBurnOut {
    @Id
    private String id;
    private Long startdate;
    private Long enddate;
    private List<MonthlyCoachingBurnOut> burnout;
}
