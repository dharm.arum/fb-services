package com.flybounce.flybounceservice.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class AttendanceCoachingBurnOut {
    @Id
    private String userId;
    private Long burnOut;
}
