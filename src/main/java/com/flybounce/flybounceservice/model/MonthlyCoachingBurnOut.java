package com.flybounce.flybounceservice.model;

import lombok.Data;

@Data
public class MonthlyCoachingBurnOut extends AttendanceCoachingBurnOut {
    private String username;
    private Long fee;
    private String bankreference;
}
